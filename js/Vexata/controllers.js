var vexataURL = vexataNameSpace.config.protocol.http + vexataNameSpace.config.host + ":" + vexataNameSpace.config.port;
var alertTime = 3000;
var eventsData = [{
			"id" : 3,
			"severity" : "Critical",
			"category" : "Logical",
			"entities" : "Volume",
			"marker" : "-",
			"description" : "Volume: MS SQL reached 90% capacity",
			"alarmuuid" : "c52d0bc5-f9d9-4d7e-b505399b699316e4",
			"acknowledged" : "false",
			"cleared" : "false",
			"occuredat" : "14 minutes ago",
			"ackowledgedat" : "timestamp",
			"clearedat" : "timestamp"
		}, {
			"id" : 9,
			"severity" : "Critical",
			"category" : "Logical",
			"entities" : "Volume",
			"marker" : "-",
			"description" : "Volume: MS SQL reached 70% capacity",
			"alarmuuid" : "c52d0bc5-f9d9-4d7e-b505399b699316e4",
			"acknowledged" : "false",
			"cleared" : "false",
			"occuredat" : "40 minutes ago",
			"ackowledgedat" : "timestamp",
			"clearedat" : "timestamp"
		}, {
			"id" : 4,
			"severity" : "Warning",
			"category" : "Logical",
			"entities" : "Volume",
			"marker" : "A",
			"description" : "Volume: Oracle HR reached 80% capacity",
			"alarmuuid" : "c52d0bc5-f9d9-4d7e-b505399b699316e3",
			"acknowledged" : "false",
			"cleared" : "false",
			"occuredat" : "4 minutes ago",
			"ackowledgedat" : "timestamp",
			"clearedat" : "timestamp"
		}, {
			"id" : 10,
			"severity" : "Warning",
			"category" : "Logical",
			"entities" : "Volume",
			"marker" : "-",
			"description" : "Volume: Oracle HR reached 70% capacity",
			"alarmuuid" : "c52d0bc5-f9d9-4d7e-b505399b699316e3",
			"acknowledged" : "false",
			"cleared" : "false",
			"occuredat" : "24 minutes ago",
			"ackowledgedat" : "timestamp",
			"clearedat" : "timestamp"
		}, {
			"id" : 5,
			"severity" : "Critical",
			"category" : "hardware",
			"entities" : "Fan",
			"marker" : "-",
			"description" : "Fan Tray 1, location 4 Failed",
			"alarmuuid" : "c52d0bc5-f9d9-4d7e-b505399ba99316e2",
			"acknowledged" : "false",
			"cleared" : "false",
			"occuredat" : "34 minutes ago",
			"ackowledgedat" : "timestamp",
			"clearedat" : "timestamp"
		}, {
			"id" : 5,
			"severity" : "warning",
			"category" : "security",
			"entities" : "User",
			"marker" : "B",
			"description" : "User: abcuser Login attempt failed from IP Address 10.1.1.2, 3 times",
			"alarmuuid" : "c52d0bc5-f9d9-4d7e-b505399b199316e2",
			"acknowledged" : "false",
			"cleared" : "false",
			"occuredat" : "54 minutes ago",
			"ackowledgedat" : "timestamp",
			"clearedat" : "timestamp"
		}];

vexataNameSpace.globalList.app.controller("dashboard", function($window, $scope, $http, $interval) {
	
	$('.collapse').on('shown.bs.collapse', function(){
		$(this).parent().find(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-up");
	}).on('hidden.bs.collapse', function(){
		$(this).parent().find(".fa-caret-up").removeClass("fa-caret-up").addClass("fa-caret-down");
	});
	$scope.iopsMetricsData = 0;
	$scope.bandwidthData = 0;
	$scope.latencyData = 0;
	
	$scope.powerStatus = {
		1 : "green",
		2 : "green",
		3 : "green",
		4 : "green",
		5 : "green",
		6 : "green"
	};
	$scope.fanStatus = {
		1 : "red",
		2 : "green",
		3 : "green",
		4 : "green",
		5 : "green",
		6 : "green",
		7 : "green",
		8 : "green",
		9 : "green",
		10 : "green",
		11 : "green",
		12 : "green",
		13 : "green",
		14 : "green",
		15 : "red",
		16 : "green",
		17 : "green",
		18 : "green",
		19 : "green",
		20 : "green",
		21 : "red",
		22 : "green",
		23 : "green",
		24 : "green",
		25 : "green",
		26 : "green",
		27 : "green",
		28 : "amber"
	};
	$scope.hbaStatus = {
		1 : "red",
		2 : "green",
		3 : "green",
		4 : "green",
		5 : "green",
		6 : "green",
		7 : "green",
		8 : "green",
		9 : "green",
		10 : "green",
		11 : "green",
		12 : "green"
	};
	$scope.esmStatus = {
		1 : "red",
		2 : "green",
		3 : "green",
		4 : "green",
		5 : "green",
		6 : "green",
		7 : "green",
		8 : "green",
		9 : "green",
		10 : "green"
	};
	
	//Mocked data for iops pointers
	$scope.iopsPointers = [{
		value : 6000,
		color : '#484F5A',
		cap : {
			size : 0.15
		}
	}, {
		value : 7500,
		color : '#5878A7',
		cap : {
			size : 0.1
		}
	}, {
		value : 0,
		color : '#969799'
	}];
	
	//Mocked data for bandwidhth pointers
	$scope.banPointers = [{
		value : 120,
		color : '#484F5A',
		cap : {
			size : 0.15
		}
	}, {
		value : 140,
		color : '#5878A7',
		cap : {
			size : 0.1
		}
	}, {
		value : 0,
		color : '#969799'
	}];
	
	//Mocked data for latency pointers
	$scope.latPointers = [{
		value : 1000,
		color : '#484F5A',
		cap : {
			size : 0.15
		}
	}, {
		value : 1500,
		color : '#5878A7',
		cap : {
			size : 0.1
		}
	}, {
		value : 0,
		color : '#969799'
	}];
	
	$interval(function() {
		var iops = Math.floor((Math.random() * 1000) + 8500);
		var bandwidth = Math.floor((Math.random() * 35) + 140);
		var latency = Math.floor((Math.random() * 400) + 1600);
		$scope.iopsRG.pointers[2].value(iops);
		$scope.bandwidthRG.pointers[2].value(bandwidth);
		$scope.latencyRG.pointers[2].value(latency);
		$scope.iopsMetricsData = iops;
		$scope.bandwidthData = bandwidth;
		$scope.latencyData = latency;
		
	}, 4000);
	//Making request to get Storage Information
	vexataNameSpace.globalList.getData($http, vexataURL + vexataNameSpace.api.storageURL, function(data, err) {
		if (data) {
			$scope.storageInfo = data;
			lineChart();
		} else {
			$scope.nodes = err;
		}
	});
	//Making request to get Port Information
	vexataNameSpace.globalList.getData($http, vexataURL + vexataNameSpace.api.portsURL, function(data, err) {
		if (data) {
			$scope.ports = data;
		} else {
			$scope.ports = err;
		}
	});
});

vexataNameSpace.globalList.app.controller("index", function($window, $scope, $http, $interval){
	$scope.criticalData = eventsData.filter(function(obj){ if(obj.severity === "Critical") return obj; });
	$scope.warningData = eventsData.filter(function(obj){ if(obj.severity === "Warning") return obj; });
	$scope.criticalData = eventsData.filter(function(obj){ if(obj.severity === "Critical") return obj; });
});
