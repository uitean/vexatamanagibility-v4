vexataNameSpace.globalList.app.config(["$stateProvider",
    "$urlRouterProvider",
    function($stateProvider, $urlRouterProvider){

        $urlRouterProvider.otherwise("/dashboard");
        $stateProvider
            .state("dashboard", {
                url:"/dashboard",
                templateUrl: vexataNameSpace.utilities.templateURL+"dashBoard.html",
                controller: "dashboard"
            })
            .state("logicalasset", {
                url:"/logicalasset",
                templateUrl: vexataNameSpace.utilities.templateURL+"logicalAssets.html",
                controller: "logicalasset"
            })
            .state("volume", {
                url:"/volume",
                templateUrl: vexataNameSpace.utilities.templateURL+"volume.html",
                controller: "volume"
            })
            .state("volumegroup", {
                url:"/volumegroup",
                templateUrl: vexataNameSpace.utilities.templateURL+"volumeGroup.html",
                controller: "volumegroup"
            })
            .state("initiatorgroup", {
                url:"/initiatorgroup",
                templateUrl: vexataNameSpace.utilities.templateURL+"initiatorGroup.html",
                controller: "initiatorgroup"
            })
            .state("targetgroup", {
                url:"/targetgroup",
                templateUrl: vexataNameSpace.utilities.templateURL+"targetGroup.html",
                controller: "targetgroup"
            })
            .state("exportgroup", {
                url:"/exportgroup",
                templateUrl: vexataNameSpace.utilities.templateURL+"exportGroup.html",
                controller: "exportgroup"
            })

    }]);
