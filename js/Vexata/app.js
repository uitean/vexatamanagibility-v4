(function () {
   	'use strict';
	 vexataNameSpace.globalList.app = angular.module('app', ['ui.router','kendo.directives']);
})();

function createChartCapacity($scope, storageData) {
    $("#chart-capacity").kendoChart({
        title: {
            position: "bottom",
            visible: false
        },
        legend: {
            position: "top",
            visible: false
        },
        chartArea: {
            background: ""
        },
        seriesDefaults: {
            labels: {
                template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                //position: "outsideEnd",
                visible: true,
                background: "transparent"
            }
        },
        series: [{
            type: "pie",
			nodeValue: storageData, //Add our own data/REST.
            startAngle: 0,
            data: [{
                category: "Used",
                value: storageData.used,
                color: "#428bca"				
            },/*{
                category: "Snapshots",
                value: 0.2,
                color: "#d9534f"
            },*/{
                category: "Free",
                value: storageData.free,
                color: "#5cb85c"
            }]
        }],
        tooltip: {
            visible: true,
			template: "#= category # - #= kendo.format('{0:P}', percentage)#",
            format: "{0}%"
        },
		
    });
}

function logarthmiChart(){
	$("#logarthmiChart").kendoChart({
                title: {
                    text: "Node IOPS & Port Bandwidth",
					visible: false
                },
                legend: {
                    position: "top"
                },
                series: [{
                    type: "column",
                    data: [3530000, 153238000, 4540000, 1232000, 2542000,7528000, 6122000, 5459000, 7744000, 5126000, 5126000, 3530000],
                    stack: true,
                    name: "Read IOPS",
                    color: "#428bca"
                }, {
                    type: "column",
                    data: [15028000, 6122000, 5459000, 7744000, 5126000, 3530000, 6238000, 4540000, 1232000, 2542000, 4540000, 1232000],
                    stack: true,
                    name: "Write IOPS",
                    color: "#5cb85c"
                }, {
                    type: "area",
                    data: [7100, 6500, 4520, 2500, 14500, 25600, 25300, 9835, 25350, 14400, 9835, 25350],
                    name: "Read Bandwidth",
                    color: "#8A74C2",
                    axis: "rbn"
                }, {
                    type: "area",
                    data: [2560, 2530, 983, 2535, 1440, 7100, 6500, 4520, 2500, 1450, 1232, 2542],
                    name: "Write Bandwidth",
                    color: "#C29D4E",
                    axis: "rbn"
                }],
                valueAxes: [ {
                    title: { text: "IOPs Count" },
                    min: 0,
                    max: 8000000
                }, {
                    name: "rbn",
                    title: { text: "Bandwidth in MBps" },
                    color: "#8A74C2",
                    min: 0,
                    max: 32000
                }],
                categoryAxis: {
                    categories: ["00:05", "00:10", "00:15", "0:20", "0:25","0:30", "0:35", "0:40", "0:45","0:50", "0:55", "1:00"],
                    // Align the first two value axes to the left
                    // and the last two to the right.
                    //
                    // Right alignment is done by specifying a
                    // crossing value greater than or equal to
                    // the number of categories.
                    axisCrossingValues: [ 0, 20, 20]
                },
				tooltip: {
	                visible: true,
	                template: "Time: #= category # - Value: #= value #",
	                format: "{0}%"
	            }
            });  
//	vexataNameSpace.globalList.logarthmiChart = $("#logarthmiChart").data("kendoChart");
}

function lineChart(title){

    var events = [{
        "size": "0",
        "win": 25,
        "event": 2
    },{
        "size": "1000",
        "win": 22,
        "event": 5
    },{
        "size": "2000",
        "win": 24,
        "event": 2
    },{
        "size": "3000",
        "win": 27,
        "eventMarker": "A",
        "event": 1
    },{
        "size": "4000",
        "win": 26,
        "event": 1
    },{
        "size": "5000",
        "win": 24,
        "event": 3
    },{
        "size": "6000",
        "win": 26,
        "event": 2
    },{
        "size": "7000",
        "win": 24,
        "eventMarker": "B",
        "event": 3
    },{
        "size": "4000",
        "win": 20,
        "event": 5
    },{
        "size": "9000",
        "win": 22,
        "event": 6
    },{
        "size": "8000",
        "win": 21,
        "event": 7
    },{
        "size": "7000",
        "win": 27,
        "event": 3
    }];

    $("#lineChart").kendoChart({
        dataSource: {
            data: events
        },
        title: {
            text: "Events in Last 1 Hour"
        },
        legend: {
            position: "bottom"
        },
        seriesDefaults: {
            type: "line"
        },
        series: [{
            field: "win",
            name: "Information",
         	color: "#428bca",
            notes: {
                label: {
                    position: "outside"
                },
                position: "bottom"
            }
        },{
            field: "event",
			color: "#5cb85c",
            name: "Critical Event",
            noteTextField: "eventMarker"
        }],
        valueAxis: {
            line: {
                visible: false
            }
        },
        categoryAxis: {
//            field: "size",
			categories: ["00:05", "00:10", "00:15", "0:20", "0:25","0:30", "0:35", "0:40", "0:45","0:50", "0:55", "1:00"],
			axisCrossingValues: [ 0, 20, 20],
            majorGridLines: {
                visible: false
            }
        },
        tooltip: {
            visible: true,
            template: "#= series.name #: #= value #"
        }
    });
//	vexataNameSpace.globalList.lineChart = $("#lineChart").data("kendoChart");
}
